module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        'roboto': ['Roboto', 'arial', 'sans-serif']
      },
      spacing: {
        '7': '1.75rem',
        '11': '2.75rem',
        '13': '3.125rem',
        '14': '3.5rem',
        '18': '4.5rem',
        '34': '8.5rem',
        '36': '9rem',
        '38': '9.5rem',
        '54': '13.5rem',
        '57': '14.375rem',
        '58': '14.5rem',
        '59': '14.750rem',
        '60': '15rem',
        '66': '17rem',
        '68': '18rem',
        '92': '23rem',
        '94': '24rem'
      },
      fontSize: {
        'tiny': '0.9375rem'
      },
      zIndex: {
       '-1': '-1',
       '51': '51'
    },
      screens: {
        'xs': '480px',
        // => @media (min-width: 480px) { ... }
        '2xl': '1600px'
        // => @media (min-width: 1600px) { ... }
      }
    },
  },
variants: { },
plugins: [],
}
